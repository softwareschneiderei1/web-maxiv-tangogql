What is GraphiQL and how can be used
====================================

GraphiQL is deployed together with tangogql, it is a graphical interactive in-browser 
GraphQL IDE used to test GraphQL queries. For more info about it check:

Source code Docs for GraphiQL: https://graphiql-test.netlify.app/typedoc/

If you deployed the taranta suite with for example https://gitlab.com/tango-controls/web/taranta-suite
GraphiQL url link should be acessible for you at: ```http://localhost:5004/graphiql/```

To check the type of queries you can use on graphiql see:  :doc:`./examples`

\ |IMG1|\ 

.. |IMG1| image:: /assets/printscreen/graphiql.png
   :width: 683 px
   :height: 273 px
   