# Tango GraphQL

A GraphQL interface for Tango.

## Description

This is an attempt at using "modern" web standards to make a TANGO web service.</br>
It provides websocket communication for subscribing to attributes, and a GraphQL interface to the TANGO database.

## Usage

The server is written in Python and currently requires python 3.6 or later.

__aiohttp__ is used for the web server part, [graphene](http://graphene-python.org/) for the GraphQL part.</br>
"requirements.txt" should list the necessary libraries, which can be installed using "pip".</br>
Also, a Conda environment can be created using the *_environment.yml*_.</br>

If preferred, a Dockerfile is provided and can be used to run the server.</br>

If the intention is to run it manually, once all the dependencies are installed, you can start the server by doing:

```shell
$ python -m tangogql
```

If you want to run the server in a read only mode, where the access to the control system is done in a read only way, you can use the environment variable: READ_ONLY, and set it to 1. If the jwt authentication is not
set up then the server can be run without authentication by setting the environment variable NO_AUTH to 1.

The requests are made to the url: http://localhost:5004/db

## Installation

At the moment of writing this, there is no packaging system ready, making the best deployment option the usage of the Docker Container.

### For development
There are two ways to run Tangogql locally for development. One can either run using Docker or using Conda/Python. If you already have a Tango database setup, you can just install the dependencies in a virtualenv (or conda env) and run the server as `python -m tangogql` (Note, it may only work on linux due to PyTango dependency). All changes below are for setting up Tangogql locally so ***do not commit these changes***
#### Common setup
Regardless of method there are some common configuration that one need to do. Follow the steps below:
1. Change WebSocket protocol in `tangogql/static/graphiql/index.html` from `/""wss:///"` to `/""ws:///"`. The change is to make sure the Graphiql connect to Tangogql WebSocket unsecure. If this is not modified the Graphiql inteface will not be able to connect to Tangogql WebSocket.
2. Install `aiohttp-devtools` by putting the below lines at the end under `dependencies` to `environment.yml`.It's recommended to create a new file as `environment-dev.yml` to avoid confusion. `aiohttp-devtools` is a useful tool for local development (and should only be used in development). This tool will restart the server every time you make changes on the `tangogql` folder. Using this you wont need to restart everything you change the code. The tool can also be pip installed after activating the conda environment.
    ```
    dependencies:
    - other libs 
    - pip
    - pip:              # put this to environment.yaml, should not commit this
        - aiohttp-devtools # put this to environment.yaml, should not commit this
    ```
3. Create a folder called `tango-test` in the same level as `tangogql` folder and in side it create file called Dockerfile. Copy the below lines to your newly created Dockerfile. This is to build the image for `tango-test`. This step can be skipped if you have `tango-test` folder somewhere.
    ```
    FROM debian

    MAINTAINER Tango Controls <info@tango-controls.org>

    # Install libtango7 & TangoTest
    RUN apt-get update && echo "$TANGO_HOST" | apt-get install -y tango-test

    ## Add the wait script to the image
    ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.7.3/wait /wait
    RUN chmod +x /wait

    ## Launch the wait tool and then your application
    CMD /wait && /usr/lib/tango/TangoTest test

    ```
5. Replace the content of `config.json` with the below lines (replace group-name with your group name e.g KITS)
    ```
    {
        "secret":"secret",
        "required_groups":["group-name"]
    }
    ```
## Running with Docker
This method is suitable for both MAC and Linux user, especially MAC user. The reason is that (21 Jul 2022) the conda-forge channel for OSX doesn't have all the required python packages so MAC user cannot run Tangogql using local Conda.
Follow the below steps:
1. Comment out `COPY . .` in Dockerfile since we already mount the `tangogql` folder so we dont want to execute this line.

2. Run `docker-compose up` and wait for it.
3. Open your browser to [localhost:5004/graphiql](http://localhost:5004/graphiql) to verify that it works.
A tool called [aiohttp-devtools](https://github.com/aio-libs/aiohttp-devtools) is used to auto-reload the server inside the Docker container whenever the code changes.

The `docker-compose.yml` file actually overwrites the start script in order to run the container with the aiohttp-devtools instead of a normal startup. This should only be used for development.
## Running with Conda
Setting up development environment with conda environment requires some modifications in the configuration.
1. Copy `docker-compose.yml` to a new file `docker-compose-dev.yml`. Comment out the `tangoql` part and add the port information:
    ```
    control-system:
    image: tangocs/tango-cs:latest
    environment: 
    ports:
    - 10000:10000
    ```

2. Put the below lines to `environment-dev.yml`. `variables` should have the same indentation as `dependencies`.

    ```
    ... other directives
    dependencies:
    - ... some libs
    variables:    # copy to environment.yml, should not be committed             
    TANGO_HOST: localhost:10000  # copy to environment.yml, should not be committed    
    ```
3. Change `host=None` to `host = os.environ["TANGO_HOST"]` in `/tangogql/aioserver.py` to get the host from environment.

4. Create conda environment
    ```shell
    $ conda env create --file environment.yml`
    ```

5. Run docker container to set up the tango test database
    ```shell
    $ docker-compose up
    ```

6. Activate the conda environment 
    ```shell
    $ conda activate tangogql
    ```

7. Start the server in watch mode (auto reloading)
    ```shell
    $ adev runserver tangogql/aioserver.py --app-factory=dev_run --port=5004
    ```
8. Open your browser to [localhost:5004/graphiql](http://localhost:5004/graphiql) to verify that it works.
A tool called [aiohttp-devtools](https://github.com/aio-libs/aiohttp-devtools) is used to auto-reload the server whenever the code changes.

## License

TangoGQL is released under the license that can be found in the LICENCE file in the root directory of the project.

## Authors

Tango GraphQL was written by the KITS Group at MAX IV Laboratory.

Special thanks to:

- Johan Forsberg and Vincent Michel
- Linh Nguyen
 
