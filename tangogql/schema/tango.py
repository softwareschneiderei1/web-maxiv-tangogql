#!/usr/bin/env python3

"""A GraphQL schema for TANGO."""

import os

import graphene
import tango

from tangogql.schema.mutations import Mutations
from tangogql.schema.query import Query
from tangogql.schema.subscription import Subscription

tango.set_green_mode(tango.GreenMode.Asyncio)

MODE = bool(os.environ.get("READ_ONLY"))

if MODE:
    mutation = None
else:
    mutation = Mutations

tangoschema = graphene.Schema(
    query=Query, mutation=mutation, subscription=Subscription
)
