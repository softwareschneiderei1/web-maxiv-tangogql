import os
from unittest.mock import MagicMock, patch

import pytest

from tangogql.tangodb import CachedDatabase, CachedMethod


@pytest.fixture(scope="session", autouse=True)
def set_tango_host():
    os.environ["TANGO_HOST"] = "localhost:10000"


def sample_method(x, y):
    return x + y


def test_cached_method():
    cm = CachedMethod(sample_method, ttl=11)
    assert cm.cache._default_ttl == 11
    assert cm.method == sample_method
    assert cm(1, 2) == 3


class TestCachedDatabase:
    @patch("tango.Database")
    def test_init(self, mock_db):
        cd = CachedDatabase(11)
        assert cd._ttl == 11

    @patch("tango.Database")
    def test_getattr(self, mock_db):
        mock_db_instance = mock_db.return_value
        mock_db_instance.get_alias = MagicMock()

        cd = CachedDatabase(11)
        assert isinstance(cd.__getattr__("get_alias"), CachedMethod)
        assert callable(cd.__getattr__("get_alias"))
        assert callable(cd.__getattr__("set_access_checked"))
