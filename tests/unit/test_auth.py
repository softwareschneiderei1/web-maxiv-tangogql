import unittest
from unittest.mock import MagicMock

from tangogql.auth import authentication, authorization


def mock_function(self, info):
    return "success"


class TestDecorators(unittest.TestCase):

    def setUp(self):
        self.mock_info = MagicMock()
        self.mock_info.context = {"config": MagicMock(), "client": MagicMock()}
        self.mock_info.context["config"].no_auth = False

    def test_authentication_no_auth(self):
        self.mock_info.context["config"].no_auth = True
        decorated_function = authentication(mock_function)
        result = decorated_function(self, self.mock_info)
        self.assertEqual(result, "success")

    def test_authentication_authenticated_user(self):
        self.mock_info.context["client"].user = "test_user"
        decorated_function = authentication(mock_function)
        result = decorated_function(self, self.mock_info)
        self.assertEqual(result, "success")

    def test_authorization_no_auth(self):
        self.mock_info.context["config"].no_auth = True
        decorated_function = authorization(mock_function)
        result = decorated_function(self, self.mock_info)
        self.assertEqual(result, "success")

    def test_authorization_authorized_user(self):
        self.mock_info.context["config"].required_groups = ["group1", "group2"]
        self.mock_info.context["client"].groups = ["group2", "group3"]
        decorated_function = authorization(mock_function)
        result = decorated_function(self, self.mock_info)
        self.assertEqual(result, "success")


if __name__ == "__main__":
    unittest.main()
