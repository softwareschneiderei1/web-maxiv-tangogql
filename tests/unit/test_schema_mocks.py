import json
import logging
import time

from tango import (
    AttrDataFormat,
    AttrQuality,
    CmdArgType,
    DbDatum,
    DevState,
    DispLevel,
    TimeVal,
)

logging.basicConfig(level=logging.DEBUG)


class GenericDataClass:
    def __init__(self, dictionary):
        for k, v in dictionary.items():
            setattr(self, k, v)


class MockCommandInfo(GenericDataClass):
    pass


class MockDeviceAttributeConfig(GenericDataClass):
    pass


class MockDeviceInfo(GenericDataClass):
    pass


class MockDatabaseDeviceInfo(GenericDataClass):
    def __init__(self, dictionary):
        super().__init__(dictionary)
        self.name = dictionary.get("class_name")


class MockTimeClass(GenericDataClass):
    def __init__(self, cur_time=time.time()):
        self.tv_sec = int(cur_time)
        self.tv_usec = int((cur_time - self.tv_sec) * 1e6)


class MockDB(object):
    def get_device_exported(self, pattern):
        device_pattern = DbDatum(pattern)
        device_pattern.value_string.append(pattern)
        logging.debug(
            f"MockDB.get_device_exported: {device_pattern.value_string}"
        )
        return device_pattern

    def get_device_domain(self, pattern):
        result = ["sys"]
        logging.debug(f"MockDB.get_device_domain: {result}")
        return result

    def get_device_family(self, url):
        result = ["tg_test"]
        logging.debug(f"MockDB.get_device_family: {result}")
        return result

    def get_device_info(self, url):
        result = MockDatabaseDeviceInfo(
            {
                "class_name": "sys/tg_test/1",
                "_class": "some enchanted class",
                "server": "sys",
                "pid": 12345,
                "started_date": "1/1/2001",
                "stopped_date": "Active",
                "exported": False,
            }
        )
        logging.debug(f"MockDB.get_device_info: {result.__dict__}")
        return result

    def get_device_member(self, pattern):
        device_member = DbDatum(pattern)
        device_member.value_string.append("1")
        logging.debug(
            f"MockDB.get_device_member: {device_member.value_string}"
        )
        return device_member

    def get_device_server_name_list(self, url):
        result = ["server"]
        logging.debug(f"MockDB.get_device_server_name_list: {result}")
        return result

    def get_device_property_list(self, name, pattern):
        result = ["property1", "property2"]
        logging.debug(f"MockDB.get_device_property_list: {result}")
        return result

    def get_device_attribute_list(self, name, pattern):
        result = ["attribute1", "attribute2"]
        logging.debug(f"MockDB.get_device_attribute_list: {result}")
        return result

    def get_device_property(self, dev_name, value):
        result = {"property1": "value1"}
        logging.debug(f"MockDB.get_device_property: {result}")
        return result

    def put_device_property(self, dev_name, value):
        result = None
        logging.debug(f"MockDB.put_device_property: {result}")
        return result

    def delete_device_property(self, dev_name, value):
        result = None
        logging.debug(f"MockDB.delete_device_property: {result}")
        return result

    def get_class_list(self, pattern):
        result = json.loads('{"data":{"classes":[{"name":"PowerSupply"}]}}')
        logging.debug(f"MockDB.get_class_list: {result}")
        return result

    def get_device_exported_for_class(self, class_name):
        result = json.loads('{"devices":[{"name":"test/power_supply/1"}]}')
        logging.debug(f"MockDB.get_device_exported_for_class: {result}")
        return result

    async def command_inout(self, command, argin):
        logging.debug(f"MockDB.command_inout: {command}, {argin}")
        if command == "DbGetDeviceWideList":
            return ["sys/tg_test/1"]
        return None


class MockProxy(object):
    def __init__(self):
        self.attrib_dict = {
            "data_format": AttrDataFormat.SCALAR,
            "data_type": CmdArgType.DevDouble,
            "disp_level": DispLevel.OPERATOR,
            "value": 200,
            "format": "%6.2f",
            "label": "ampli",
            "max_dim_x": 1,
            "max_dim_y": 0,
            "name": "ampli",
            "writable": True,
            "unit": "",
            "description": "",
            "min_value": 0,
            "max_value": 0,
            "min_alarm": 0,
            "max_alarm": 0,
            "w_value": 0,
            "quality": AttrQuality.ATTR_VALID,
            "time": MockTimeClass(),
            "timestamp": TimeVal(0, 0, 0),
            "enum_labels": ["label 1", "label 2"],
            "info": "",
        }

        self.mockAttribConfig = MockDeviceAttributeConfig(self.attrib_dict)

    def attribute_query(self, name):
        logging.debug(
            f"MockProxy.attribute_query: {self.mockAttribConfig.__dict__}"
        )
        return self.mockAttribConfig

    def attribute_list_query(self):
        result = [self.mockAttribConfig]
        logging.debug(f"MockProxy.attribute_list_query: {result}")
        return result

    def command_list_query(self):
        result = [
            MockCommandInfo(
                {
                    "cmd_name": "DevBoolean",
                    "disp_level": DispLevel.OPERATOR,
                    "in_type": CmdArgType.DevBoolean,
                    "out_type": CmdArgType.DevBoolean,
                    "cmd_tag": 0,
                    "in_type_desc": "Any boolean value",
                    "out_type_desc": "Echo of the argin value",
                }
            )
        ]
        logging.debug(f"MockProxy.command_list_query: {result}")
        return result

    def dev_name(self):
        result = "sys/tg_test/1"
        logging.debug(f"MockProxy.dev_name: {result}")
        return result

    def name(self):
        result = "sys/tg_test/1"
        logging.debug(f"MockProxy.name: {result}")
        return result

    async def state(self):
        result = DevState.ON
        logging.debug(f"MockProxy.state: {result}")
        return result

    def alias(self):
        result = ""
        logging.debug(f"MockProxy.alias: {result}")
        return result

    def info(self):
        result = MockDeviceInfo(
            {
                "dev_class": "TangoTest",
                "doc_url": "http://www.esrf.eu/computing/cs/tango/tango_doc/",
                "server_host": "buster",
                "server_id": "TangoTest/test",
                "server_version": 5,
            }
        )
        logging.debug(f"MockProxy.info: {result.__dict__}")
        return result

    async def read_attribute(self, name, extract_as):
        logging.debug(
            f"MockProxy.read_attribute: {self.mockAttribConfig.__dict__}"
        )
        return self.mockAttribConfig

    @staticmethod
    async def command_inout(command, argin):
        logging.debug(f"MockProxy.command_inout: {command}, {argin}")
        return None

    async def write_read_attribute(self):
        logging.debug(
            f"MockProxy.write_read_attribute: {self.mockAttribConfig.__dict__}"
        )
        return self.mockAttribConfig


class MockDeviceProxyCache(object):
    @staticmethod
    def get():
        result = MockProxy()
        logging.debug(f"MockDeviceProxyCache.get: {result}")
        return result
