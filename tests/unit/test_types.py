import importlib.util
import sys
import unittest

from graphql.language import ast

# Load the custom types module explicitly
spec = importlib.util.spec_from_file_location(
    "custom_types", "tangogql/schema/types.py"
)
custom_types = importlib.util.module_from_spec(spec)
sys.modules["custom_types"] = custom_types
spec.loader.exec_module(custom_types)


class TestScalarTypes(unittest.TestCase):

    def test_coerce_type_devstate(self):
        class DevState:
            def __str__(self):
                return "DevState"

        value = DevState()
        result = custom_types.ScalarTypes.coerce_type(value)
        self.assertEqual(result, "DevState")

    def test_coerce_type_float_inf(self):
        value = float("inf")
        result = custom_types.ScalarTypes.coerce_type(value)
        self.assertEqual(result, "inf")

    def test_coerce_type_float(self):
        value = 3.14
        result = custom_types.ScalarTypes.coerce_type(value)
        self.assertEqual(result, 3.14)

    def test_serialize(self):
        value = "test"
        result = custom_types.ScalarTypes.serialize(value)
        self.assertEqual(result, "test")

    def test_parse_value(self):
        value = "test"
        result = custom_types.ScalarTypes.parse_value(value)
        self.assertEqual(result, "test")

    def test_parse_literal_int(self):
        node = ast.IntValue(value="42")
        result = custom_types.ScalarTypes.parse_literal(node)
        self.assertEqual(result, 42)

    def test_parse_literal_float(self):
        node = ast.FloatValue(value="3.14")
        result = custom_types.ScalarTypes.parse_literal(node)
        self.assertEqual(result, 3.14)

    def test_parse_literal_boolean(self):
        node = ast.BooleanValue(value=True)
        result = custom_types.ScalarTypes.parse_literal(node)
        self.assertEqual(result, True)

    def test_parse_literal_list(self):
        node = ast.ListValue(
            values=[ast.IntValue(value="1"), ast.IntValue(value="2")]
        )
        result = custom_types.ScalarTypes.parse_literal(node)
        self.assertEqual(result, [1, 2])

    def test_parse_literal_string(self):
        node = ast.StringValue(value="test")
        result = custom_types.ScalarTypes.parse_literal(node)
        self.assertEqual(result, "test")

    def test_parse_literal_invalid(self):
        node = ast.ObjectValue(fields=[])
        result = custom_types.ScalarTypes.parse_literal(node)
        self.assertIsInstance(result, ValueError)


class TestTypeConverter(unittest.TestCase):

    def test_convert_devboolean_true(self):
        result = custom_types.TypeConverter.convert("DevBoolean", "true")
        self.assertEqual(result, True)

    def test_convert_devboolean_false(self):
        result = custom_types.TypeConverter.convert("DevBoolean", "false")
        self.assertEqual(result, False)

    def test_convert_devboolean_invalid(self):
        result = custom_types.TypeConverter.convert("DevBoolean", "invalid")
        self.assertIsInstance(result, ValueError)

    def test_convert_devfloat(self):
        result = custom_types.TypeConverter.convert("DevFloat", "3.14")
        self.assertEqual(result, 3.14)

    def test_convert_devint(self):
        result = custom_types.TypeConverter.convert("DevInt", "42")
        self.assertEqual(result, 42)

    def test_convert_devstring(self):
        result = custom_types.TypeConverter.convert("DevString", "test")
        self.assertEqual(result, "test")

    def test_convert_invalid_type(self):
        result = custom_types.TypeConverter.convert("InvalidType", "test")
        self.assertEqual(result, "test")


if __name__ == "__main__":
    unittest.main()
