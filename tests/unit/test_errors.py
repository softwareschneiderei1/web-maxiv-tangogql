from unittest.mock import Mock

import PyTango
import pytest
from graphql.error import GraphQLLocatedError
from tango import DevError, DevFailed, ErrSeverity


@pytest.fixture
def error():
    df = Mock(spec=DevError)
    df.severity = ErrSeverity.ERR
    return df


class ErrorParser:
    @staticmethod
    def remove_duplicated_errors(errors):
        seen = set()
        result_set = []
        for message in errors:
            if message["reason"] != "None":
                if isinstance(message, dict):
                    t = tuple(sorted(message.items()))
                else:
                    t = str(message)
                if t not in seen:
                    seen.add(t)
                    result_set.append(message)
        return result_set

    @staticmethod
    def parse(error):
        message = {}
        if hasattr(error, "original_error"):
            if isinstance(error.original_error, tuple):
                error_original_error = error.original_error[0]
            else:
                error_original_error = error.original_error

            if isinstance(
                error_original_error,
                (
                    PyTango.ConnectionFailed,
                    PyTango.CommunicationFailed,
                    PyTango.DevFailed,
                ),
            ):
                for e in error_original_error.args:
                    if e.reason == "" or e.reason == "API_CorbaException":
                        pass
                    elif e.reason in [
                        "API_CantConnectToDevice",
                        "API_DeviceTimedOut",
                    ]:
                        message["device"] = e.desc.split("\n")[0].split(" ")[
                            -1
                        ]
                        message["desc"] = e.desc.split("\n")[0]
                        message["reason"] = e.reason.split("_")[-1]
                    elif e.reason == "API_AttributeFailed":
                        [device, attribute] = e.desc.split(",")
                        message["device"] = device.split(" ")[-1]
                        message["attribute"] = attribute.split(" ")[-1]
                    elif e.reason == "API_AttrValueNotSet":
                        message["reason"] = e.reason.split("_")[-1]
                        message["field"] = e.desc.split(" ")[1]
                    else:
                        message["reason"] = e.reason
                        message["desc"] = e.desc
            else:
                message["reason"] = str(error)
        else:
            message["reason"] = str(error)
        return message

    @staticmethod
    def test_parser():
        error_parser = ErrorParser()

        error_cant_connect = Mock(spec=DevError)
        error_device_timeout = Mock(spec=DevError)
        error_attr_failed = Mock(spec=DevError)
        error_attr_not_set = Mock(spec=DevError)
        error_random_reason = Mock(spec=DevError)
        error_empty_reason = Mock(spec=DevError)
        error_corba_reason = Mock(spec=DevError)

        error_cant_connect.reason = "API_CantConnectToDevice"
        error_cant_connect.origin = "Lorem ipsum"
        error_cant_connect.desc = "Can't connect to device sys/tg_test/1"

        error_device_timeout.reason = "API_DeviceTimedOut"
        error_device_timeout.origin = "Lorem ipsum"
        error_device_timeout.desc = "Device timeout sys/tg_test/2"

        error_attr_failed.reason = "API_AttributeFailed"
        error_attr_failed.origin = "Lorem ipsum"
        error_attr_failed.desc = "Attribute failed sys/tg_test/1,long_scalar"

        error_attr_not_set.reason = "API_AttrValueNotSet"
        error_attr_not_set.origin = "Lorem ipsum"
        error_attr_not_set.desc = "Attribute long_scalar is not set"

        error_random_reason.reason = "API_RandomRandom"
        error_random_reason.origin = "Lorem ipsum"
        error_random_reason.desc = "Random desc"

        error_empty_reason.reason = ""
        error_empty_reason.origin = "Lorem ipsum"
        error_empty_reason.desc = "Random desc"

        error_corba_reason.reason = "API_CorbaException"
        error_corba_reason.origin = "Lorem ipsum"
        error_corba_reason.desc = "Random desc"

        parent_error = GraphQLLocatedError([], DevFailed(error_cant_connect))
        parsed_error = error_parser.parse(parent_error)
        assert parsed_error["device"] == "sys/tg_test/1"
        assert parsed_error["desc"] == "Can't connect to device sys/tg_test/1"
        assert parsed_error["reason"] == "CantConnectToDevice"

        parent_error = GraphQLLocatedError([], DevFailed(error_device_timeout))
        parsed_error = error_parser.parse(parent_error)
        assert parsed_error["device"] == "sys/tg_test/2"

        parent_error = GraphQLLocatedError([], DevFailed(error_attr_failed))
        parsed_error = error_parser.parse(parent_error)
        assert parsed_error["device"] == "sys/tg_test/1"
        assert parsed_error["attribute"] == "long_scalar"

        parent_error = GraphQLLocatedError([], DevFailed(error_attr_not_set))
        parsed_error = error_parser.parse(parent_error)
        assert parsed_error["reason"] == "AttrValueNotSet"
        assert parsed_error["field"] == "long_scalar"

        parent_error = GraphQLLocatedError([], DevFailed(error_random_reason))
        parsed_error = error_parser.parse(parent_error)
        assert parsed_error["reason"] == "API_RandomRandom"
        assert parsed_error["desc"] == "Random desc"

        parent_error = GraphQLLocatedError([], DevFailed(error_empty_reason))
        parsed_error = error_parser.parse(parent_error)
        assert not parsed_error.items()

        parent_error = GraphQLLocatedError([], DevFailed(error_corba_reason))
        parsed_error = error_parser.parse(parent_error)
        assert not parsed_error.items()

        parent_error = DevFailed()
        parsed_error = error_parser.parse(parent_error)
        assert parsed_error["reason"] == "DevFailed[\n]"

        parent_error = GraphQLLocatedError([], NameError("test"))
        parsed_error = error_parser.parse(parent_error)
        assert parsed_error["reason"] == "test"

    @staticmethod
    def test_remove_duplicated_errors():
        error_parser = ErrorParser

        error_cant_connect = Mock(spec=DevError)
        error_device_timeout = Mock(spec=DevError)

        error_cant_connect.reason = "API_CantConnectToDevice"
        error_cant_connect.origin = "Lorem ipsum"
        error_cant_connect.desc = "Can't connect to device sys/tg_test/1"

        error_device_timeout.reason = "API_DeviceTimedOut"
        error_device_timeout.origin = "Lorem ipsum"
        error_device_timeout.desc = "Device timeout sys/tg_test/2"

        errors = [
            error_parser.parse(
                GraphQLLocatedError([], DevFailed(error_cant_connect))
            ),
            error_parser.parse(
                GraphQLLocatedError([], DevFailed(error_device_timeout))
            ),
            error_parser.parse(
                GraphQLLocatedError([], DevFailed(error_cant_connect))
            ),
        ]

        errors_set = error_parser.remove_duplicated_errors(errors)
        assert len(errors_set) == 2
        assert all(isinstance(x, dict) for x in errors_set)

        error_attr_failed = Mock(spec=DevError)
        error_attr_failed.reason = "API_AttributeFailed"
        error_attr_failed.origin = "Lorem ipsum"
        error_attr_failed.desc = "Attribute failed sys/tg_test/1,long_scalar"

        errors.append(
            error_parser.parse(
                GraphQLLocatedError([], DevFailed(error_attr_failed))
            )
        )
        errors.append(
            error_parser.parse(
                GraphQLLocatedError([], DevFailed(error_attr_failed))
            )
        )

        errors_set = error_parser.remove_duplicated_errors(errors)
        assert len(errors_set) == 3
        assert all(isinstance(x, dict) for x in errors_set)


if __name__ == "__main__":
    pytest.main()
